"""Module for ML service on FastAPI"""

import os
import sys
import warnings
import pandas as pd
import mlflow
from dotenv import load_dotenv
from fastapi import FastAPI, File, UploadFile, HTTPException
from fastapi.responses import FileResponse

PROJECT_VER = "0.1.0"

# Load the environment variables from the .env file into the application
load_dotenv(override=True)
logged_model = 'runs:/4ad3763f830b479a80ad8872c5916934/models/model.pickle'
user_id = 1223851
warnings.filterwarnings("ignore")

# Initialize the FastAPI application
app = FastAPI()


# Create the POST endpoint with path '/invocations'
@app.post("/invocations", response_class=FileResponse)
async def create_upload_file(file: UploadFile = File(...)):
    """Generates .csv file of the submission format by input .csv file of
    the original format.

    Args:
        file (UploadFile, optional): file .csv
            Defaults to File(...).

    Raises:
        HTTPException: If file not .csv

    Returns:
        FileResponse: Return File Response (file in .csv format)
    """
    # Handle the file only if it is a CSV
    if file.filename.endswith(".csv"):  # pylint: disable=no-else-return
        # Create a temporary file with the same name as the uploaded
        # CSV file to load the data into a pandas Dataframe
        with open(file.filename, "wb") as input_file:
            input_file.write(file.file.read())

        data = pd.read_csv(file.filename)

        # Return a JSON object containing the model predictions
        os.remove(file.filename)

        # response file path
        response_file_path = "submission.csv"
        predictions = []
        # Load model as a PyFuncModel.
        print(logged_model)
        loaded_model = mlflow.pyfunc.load_model(logged_model)
        print(user_id)
        recipes_ids = data["recipe_id"].values

        for recipe_id in recipes_ids:
            predictions.append(loaded_model.predict([user_id, recipe_id]))
        print(predictions)
        # Predict on a Pandas DataFrame.
        pd_pred = pd.DataFrame(predictions)
        pd_pred.to_csv(response_file_path, index=False)
        return response_file_path

    else:
        # Raise a HTTP 400 Exception, indicating Bad Request
        # (you can learn more about HTTP response status codes here)
        raise HTTPException(
            status_code=400,
            detail="Invalid file format. Only CSV Files accepted."
            )


# Check if the environment variables for AWS access are available.
# If not, exit the program
if (
    os.getenv("AWS_ACCESS_KEY_ID") is None or
    os.getenv("AWS_SECRET_ACCESS_KEY") is None
     ):
    sys.exit(1)
