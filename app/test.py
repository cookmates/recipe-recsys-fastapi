
import warnings
import pandas as pd
import mlflow
from dotenv import load_dotenv

PROJECT_VER = "0.1.0"

logged_model = 'runs:/4ad3763f830b479a80ad8872c5916934/models/model.pickle'

warnings.filterwarnings("ignore")

# Load the environment variables from the .env file into the application
load_dotenv(override=True)
# os.environ["MLFLOW_S3_ENDPOINT_URL"] = os.getenv("MLFLOW_S3_ENDPOINT_URL")

filename = './app/example.csv'

data = pd.read_csv(filename)

# Return a JSON object containing the model predictions
# os.remove(filename)

# response file path
response_file_path = "app/_test.csv"
predictions = []
# Load model as a PyFuncModel.
loaded_model = mlflow.pyfunc.load_model(logged_model)
user_id = 1223851
recipes_ids = data["recipe_id"].values

for recipe_id in recipes_ids:
    predictions.append(loaded_model.predict([user_id, recipe_id]))

# Predict on a Pandas DataFrame.
pd_pred = pd.DataFrame(predictions)
pd_pred.to_csv(response_file_path, index=False)
