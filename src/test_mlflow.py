import mlflow
from src import mlflow_set_tracking_config

mlflow_set_tracking_config()

mlflow.set_tracking_uri("postgres_uri")  # replace
expr_name = "ex2"
s3_bucket = "s3_bucket_uri"  # replace

mlflow.create_experiment(expr_name, s3_bucket)
mlflow.set_experiment(expr_name)

with mlflow.start_run():
    # debug
    print(mlflow.get_artifact_uri())
    print(mlflow.get_tracking_uri())

    mlflow.log_param("foo", 0)
    mlflow.log_metric("bar", 1)

    with open("test.txt", "w") as f:
        f.write("test")

    mlflow.log_artifact("test.txt")
