import os
from dotenv import load_dotenv
import mlflow


PROJECT_VER = '0.1.0'


def mlflow_set_tracking_config(
    experiment_name: str = "Default", project_name: str = "recipes-recsys"
) -> None:
    """Sets up experiment tracking via MLFlow Tracking server.
     Implements scenario 4. The .env file must contain the following variables:
     MLFLOW_TRACKING_URI - MLflow server tracking URL (for example
     "http://localhost:5000")

     MLFLOW_S3_ENDPOINT_URL* - URL to s3 repository
     (eg "http://localhost:9000")
     AWS_ACCESS_KEY_ID* - key ID to s3 storage (with write permissions)
     AWS_SECRET_ACCESS_KEY* - secret key to s3 storage
     * used by boto3 library


     Args:
         experiment_name (str, optional): The name of a series of experiments
         in MLflow.
             Defaults to "default".
         project_name (str, optional): The name of the current project, usually
         matches
             [tool.poetry].name from pyproject.toml.
    """
    load_dotenv(override=True)
    mlflow.set_tracking_uri(os.getenv("MLFLOW_TRACKING_URI"))
    # mlflow.set_registry_uri(os.getenv("MLFLOW_S3_ENDPOINT_URL"))
    # project_ver = pkg_resources.get_distribution(project_name).version
    project_ver = PROJECT_VER
    mlflow.set_experiment(f"{experiment_name}_v_{project_ver}")
