from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.1',
    description='Recipe recommendation service',
    author='Anna Vasilenko',
    license='BSD-3',
)
