# -*- coding: utf-8 -*-
import click
import logging
import mlflow
import pandas as pd
from surprise import BaselineOnly, Dataset, dump, Reader
from src import mlflow_set_tracking_config
from src.models import predict_model
from src.models import SurpriseWrapper
from src.metrics import metrics_evaluate
from src.config import PATH_PROCESSED
# from src.config import PATH_MODELS


mlflow_set_tracking_config()

FILE_PATH = "data/processed/predictions_test.csv"
MODEL = "models/model.pickle"


@click.command()
@click.argument("input_filepath",
                default=f"{PATH_PROCESSED}/interactions_train.csv",
                type=click.Path(exists=True))
@click.argument("output_filepath",
                default="models",
                type=click.Path())
def main(input_filepath: str,
         output_filepath: str) -> None:
    """
    Load preprocessed interactions dataset from (../processed)
    and train model (saved in ./models).
    """
    logger = logging.getLogger(__name__)
    logger.info("training model on processed data")

    # Load dataset
    df_interactions = pd.read_csv(input_filepath)
    reader = Reader()
    train_data = Dataset.load_from_df(df_interactions[["user_id",
                                                       "recipe_id",
                                                       "rating"]],
                                      reader)
    trainset = train_data.build_full_trainset()

    # Fit model
    model = BaselineOnly()
    model.fit(trainset)
    path = f"{output_filepath}/model.pickle"
    
    # Save model
    dump.dump(path, algo=model)

    predictions = predict_model(
        FILE_PATH,
        MODEL
        )
    predictions.to_csv(FILE_PATH, index=False)

    metrics = metrics_evaluate(FILE_PATH)

    # Mlflow tracking
    artifacts = {"surprise_model": MODEL}
    mlflow.pyfunc.log_model(
        artifact_path=MODEL,
        python_model=SurpriseWrapper(),
        artifacts=artifacts
        )
    mlflow.log_metrics(metrics)


if __name__ == "__main__":

    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    main()
