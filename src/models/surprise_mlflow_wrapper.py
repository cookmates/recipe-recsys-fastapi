# Load training and test datasets
import mlflow.pyfunc
from typing import Any
from surprise import dump


FILE_PATH = "data/processed/predictions_test.csv"
MODEL = "models/model.pickle"

PYTHON_VERSION = "{major}.{minor}.{micro}".format(major="0",
                                                  minor="1",
                                                  micro="0")

artifacts = {"surprise_model": MODEL}


# create wrapper
class SurpriseWrapper(mlflow.pyfunc.PythonModel):  # type: ignore
    """
    Class to train and use Surprise Model
    """
    def load_context(self, context) -> None:  # type: ignore
        """This method is called when loading an MLflow model with
        pyfunc.load_model(), as soon as the Python Model is constructed.
        Args:
            context: MLflow context where the model artifact is stored.
        """
        # load model
        _, model = dump.load(context.artifacts["surprise_model"])

        self.model = model

    def predict(self, context, model_input) -> Any:  # type: ignore
        """This is an abstract function. We customized it into a method to
        fetch the FastText model.
        Args:
            context ([type]): MLflow context where the model artifact
            is stored.
            model_input ([type]): the input data to fit into the model.
        Returns:
            [type]: the loaded model artifact.
        """
        return self.model.predict(model_input[0], model_input[1])
