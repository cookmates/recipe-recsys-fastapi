
Данный репозиторий создан в рамках выполнения домашней работы при прохождении курса **"MLOps и production подход к ML исследованиям 2.0"** 
В репозитории реализован пример сервинга модели с использованием Docker и FastAPI.
В рамках ДЗ было решено ограничиться только сервисом готовой модели (predict) для решения задачи знакомства с технологиями 
взаимодействия контейнеров Docker с использованием MLFlow и FastApi.
В итоговом проекте планируется перенесение в облачный сервис с локальной машины и создание полноценного сервиса с базой данных пользователей и создания рекомендаций с помощью telegram-бота.

### Инструкция по запуску всей инфраструктуры проекта на новой машине (Ubuntu):

- Требуются установленные в системе:
	- python 3.11.2
	- poetry
	- Docker

Все описываемые в курсе шаги были реализованы на локальной машине, поэтому воспроизвести код из данного проекта можно только запустив предварительно код для загрузки файлов и все сервисы для реализации логирования результатов обучения модели в MLFlow.


Для этого нужно скопировать проект с построением рекомендательной системы (включая анализ данных и создание Docker контейнеров с MLFlow):

- git clone https://gitlab.com/cookmates/recipes_recsys.git
- все нижеуказанные команды выполняем находясь в корневой директории данного проекта
- создаем в корне проекта файл .env следующего содержания:
```
POSTGRES_DB=recsys_db
POSTGRES_USER=user
POSTGRES_PASSWORD=user_pass
POSTGRES_HOST=postgres
PGADMIN_DEFAULT_EMAIL=email@email.com
PGADMIN_DEFAULT_PASSWORD=pgadmin_pass
KAGGLE_USERNAME=<your_kaggle_username>
KAGGLE_KEY=<your_kaggle_key>
AWS_S3_MLFLOW_BUCKET = mlflow
AWS_S3_DVC_BUCKET = dvc
MLFLOW_S3_ENDPOINT_URL="http://localhost:9000"
MLFLOW_TRACKING_URI = "http://localhost:5001"
MINIO_ROOT_USER = user
MINIO_ROOT_PASSWORD = user_pass_minio
AWS_ACCESS_KEY_ID = <ACCESS_KEY_ID>
AWS_SECRET_ACCESS_KEY = <SECRET_ACCESS_KEY>
```
- создаем файл .dvc/config.local следующего содержания
```
['remote "s3minio"']
    access_key_id = AWS_ACCESS_KEY_ID
    secret_access_key = AWS_SECRET_ACCESS_KEY
```
- docker-compose --env-file .env up --build (дожидаемся старта контейнеров)
- производим предварительную настройку minio s3
	- Заходим через браузер http://127.0.0.1:9001 и авторизуемся user/user_pass_minio
	- Через веб-интерфейс создаем два бакета:
		- Buckets -> Create bucket
			- mlflow
			- dvc
	- Через веб-интерфейс создаем сервисный аккаунт (по сути API Key):
		- Identity -> Service Accounts -> Create service account
		- Name: <ACCESS_KEY_ID>
		- Pass: <SECRET_ACCESS_KEY>
- Создаем виртуальную среду
	- poetry config virtualenvs.in-project true
	- poetry install
	- poetry shell
- Добавляем в проект исходные данные (датасеты), для этого выполнить скрипт:
	- src/data/load_data.py
- Запускаем эксперимент
	- dvc repro
Пушим все закэшированное содержимое в локальный s3 (докер контейнеры minio и nginx должны быть запущены)
	- dvc push
- Заходим в mlflow через http://127.0.0.1:5001 
	- проверяем, что трекинг эксперимента успешно добавлен
	- заходим в модели и проводим регистрацию модели, затем для текущей модели stage = staging


Далее работаем с репозиторием Recipe recsys fastapi.

- git clone https://gitlab.com/cookmates/recipe-recsys-fastapi.git

Cоздаем .env файл:

AWS_ACCESS_KEY_ID = <ACCESS_KEY_ID>
AWS_SECRET_ACCESS_KEY = <SECRET_ACCESS_KEY>
MLFLOW_S3_ENDPOINT_URL="http://<ip_host_PC>:9000"  (где ip_host_PC отличается от 127.0.0.1)
MLFLOW_TRACKING_URI = "http://<ip_host_PC>:5001"  (где ip_host_PC отличается от 127.0.0.1)

Инициализация модели происходит в app/inference.py:
mlflow.pyfunc.load_model("runs:/<artifactID>/models/model.pickle") - в репо реализован данный вариант, можно поменять на:
mlflow.pyfunc.load_model(f"models:/{model_name}/{model_stage}"), т.е.:
mlflow.pyfunc.load_model(f"models:/{default}/{Staging}")
Тогда при необходимости деплоя новой версии модели выставляется необходимый stage в mlflow, далее запускается команда пересборки контейнера.
Модель инициализируется внутри функции обработчика POST запроса, при необходимости обновления,
достаточно изменить стейдж в mlflow, и при следующей сборке уже подтянется новая модель; 
данный вариант подходит для легковесных моделей и малонагруженных сервисов.

Собираем образ:
- docker-compose --env-file .env up --build

- По итогу должны получить работающий контейнер ml_service и API по адресу http://127.0.0.1:8004/invocations:
- Проверить работу API можно через postman
	- POST
	- http://127.0.0.1:8000/invocations
	- BODY -> from-data
	- в первой строке, в поле key вводим "file" (это имя параметра функции async def create_upload_file(file: UploadFile = File(...))), в этой же ячейке справа в выпадающем списке выбираем File
	- указываем предварительно подготовленный файл example.csv (app/example.csv) с несколькими строками исходного формата датафрейма
	для predict.

	- нажимаем send, по итогу должны получить ответ в виде .csv файла, для примера выше ответ будет следующим:
```
uid,iid,r_ui,est,details
1223851,371412.0,,4.644638429426078,{'was_impossible': False}
1223851,82486.0,,4.856528311246916,{'was_impossible': False}
1223851,93056.0,,4.630576848407329,{'was_impossible': False}
1223851,406845.0,,4.771760679025306,{'was_impossible': False}
1223851,106499.0,,4.501572856974987,{'was_impossible': False}
1223851,179839.0,,4.840286042586305,{'was_impossible': False}
1223851,412447.0,,4.753410218953216,{'was_impossible': False}
1223851,115755.0,,4.837047345723124,{'was_impossible': False}
1223851,177017.0,,4.887135653324528,{'was_impossible': False}
1223851,287223.0,,4.851538114245406,{'was_impossible': False}
1223851,2496.0,,4.882053743578889,{'was_impossible': False}
1223851,409671.0,,4.670451528134857,{'was_impossible': False}
1223851,325177.0,,4.751861128735514,{'was_impossible': False}
1223851,269624.0,,4.8246617240565115,{'was_impossible': False}
1223851,334898.0,,4.688379856576499,{'was_impossible': False}
1223851,19678.0,,4.962154280306352,{'was_impossible': False}
1223851,402528.0,,4.64716193662108,{'was_impossible': False}
1223851,204901.0,,4.768906753431286,{'was_impossible': False}
1223851,55110.0,,4.71484377144231,{'was_impossible': False}

```
